﻿/*
 * Created by SharpDevelop.
 * User: arawson
 * Date: 1/1/2015
 * Time: 5:36 PM
 */
using System;
using System.Collections.Generic;

namespace EulerWasSharp
{
	public class Problem5 : ISolvable
	{
		private bool solved = false;
		private long solution = 0;
		
		public string Name {
			get { return "Problem 5: Smallest Multiple"; }
		}
		
		public string Description {
			get { return "2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder."
					+ "\n\nWhat is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?";
			}
		}
		
		public string Solution {
			get {
				if (!solved)
				{
					/*this small lcm problem can be solved by hand, so that can be used to check our answer
					 * 20 = 2^2 * 5
					 * 19 = 19^1
					 * 18 = 3^2 * 2
					 * 17 = 17^1
					 * 16 = 2^4
					 * 15 = 3 * 5
					 * 14 = 7 * 2
					 * 13 = 13
					 * 12 = 2^4 * 3
					 * 11 = 11
					 * 10 = 5 * 2
					 * 9 = 3^2
					 * 8 = 2^3
					 * 7 = 7
					 * 6 = 2 * 3
					 * 5 = 5
					 * 4 = 2^2
					 * 3 = 3
					 * 2 = 2
					 * 1 = 1
					 * 
					 * so what is the maximum exponent of each prime number in the set?
					 * 2 ^ 4
					 * 3 ^ 2
					 * 5 ^ 1 //every prime after this point can be a factor in the sequence at most once
					 * 7
					 * 11
					 * 13
					 * 17
					 * 19
					 * the product of which (the lcm) is 232792560
					 */
					
					const int limit = 20;
					solution = 1;
					
					foreach(int f in Util.GetPrimesTo(limit)) {
						int exponent = FirstExponentBelow(f, limit);
						Console.WriteLine("" + f + " ^ " + exponent);
						solution *= (int)Math.Pow(f, exponent);
					}
					
					solved = true;
				}
				return "" + solution;
			}
		}
		
		private static int FirstExponentBelow(int b, int limit) {
			int exponent = -1;
			int value = 1;
			while (value < limit) {
				value *= b;
				exponent += 1;
			}
			return exponent;
		}
	}
}
