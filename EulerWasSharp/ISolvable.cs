﻿/*
 * Created by SharpDevelop.
 * User: arawson
 * Date: 12/31/2014
 * Time: 6:01 PM
 */
using System;

namespace EulerWasSharp
{
	/// <summary>
	/// Description of ISolvable.
	/// </summary>
	public interface ISolvable
	{
		string Name {
			get;
		}

		string Description {
			get;
		}

		string Solution {
			get;
		}
	}
}
