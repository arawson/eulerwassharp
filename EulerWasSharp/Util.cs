﻿/*
 * Created by SharpDevelop.
 * User: arawson
 * Date: 1/1/2015
 * Time: 4:58 PM
 */
using System;
using System.Collections.Generic;

namespace EulerWasSharp
{
	public class Util
	{
		public static bool IsPalindrome(string s)
		{
			for (int i = 0; i < s.Length; i++) {
				if (s[i] != s[s.Length - 1 - i]) {
					return false;
				}
			}
			return true;
		}
		
		public static bool IsPalindrome(int n)
		{
			return IsPalindrome("" + n);
		}
		
		public static List<int> GetPrimesTo(int n) {
			List<int> primes = new List<int>();
			primes.Add(2);
			primes.Add(3);
			
			for (int factor = 5; factor < n; factor+=2) {
				bool prime = true;
				foreach (int f2 in primes) {
					if (factor % f2 == 0) {
						prime = false;
						break;
					}
				}
				if (prime) primes.Add(factor);
			}
			return primes;
		}
		
		public static int GetPrime(int n) {
			List<int> primes = new List<int>();
			primes.Add(2);
			primes.Add(3);
			
			int at = 3;
			int test = 3;
			
			while (at <= n) {
				test += 2;
				bool prime = true;
				foreach (int p in primes) {
					if (test % p == 0) {
						prime = false;
						break;
					}
				}
				
				if (prime) {
					at++;
					primes.Add(test);
				}
			}
			
			return test;
		}
	}
}
