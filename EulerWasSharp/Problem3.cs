﻿/*
 * Created by SharpDevelop.
 * User: arawson
 * Date: 1/1/2015
 * Time: 3:36 PM
 */
using System;
using System.Collections.Generic;

namespace EulerWasSharp
{
	public class Problem3 : ISolvable
	{
		private bool solved = false;
		private long solution = 0;
		
		public string Name {
			get { return "Problem 3: Largest Prime Factor"; }
		}
		
		public string Description {
			get { return "The prime factors of 13195 are 5, 7, 13 and 29."
					+"\n\nWhat is the largest prime factor of the number 600851475143 ?";
			}
		}
		
		public string Solution {
			get {
				if (!solved) {
					//(not so) quick and easy sieve of eratosthanes
					const long number = 600851475143;
					
					List<long> primes = new List<long>();
					long end = (long)Math.Sqrt(number);
					primes.Add(2);
					primes.Add(3);
					
					for (long factor = 5; factor < end; factor+=2) {
						bool prime = true;
						foreach (long f2 in primes) {
							if (factor % f2 == 0) {
								prime = false;
								break;
							}
						}
						if (prime) primes.Add(factor);
					}
					
					foreach (long f in primes) {
						if (number % f == 0) {
							solution = f;
						}
					}
				}
				
				return "" + solution;
			}
		}
	}
}
