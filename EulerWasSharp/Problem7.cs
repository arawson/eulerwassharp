﻿/*
 * Created by SharpDevelop.
 * User: arawson
 * Date: 1/4/2015
 * Time: 4:02 PM
 */
using System;

namespace EulerWasSharp
{
	public class Problem7 : ISolvable
	{
		bool solved = false;
		int solution = 0;
		
		public string Name {
			get { return "Problem 7: 10001st Prime"; }
		}
		
		public string Description {
			get { return "By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.\n\n"
					+ "What is the 10 001st prime number?"; }
		}
		
		public string Solution {
			get {
				if (!solved) {
					solution = Util.GetPrime(10001);
					
					solved = true;
				}
				
				return "" + solution;
			}
		}
	}
}
