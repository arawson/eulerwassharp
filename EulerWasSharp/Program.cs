﻿/*
 * Created by SharpDevelop.
 * User: arawson
 * Date: 12/31/2014
 * Time: 6:00 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using Gtk;
using System.Collections.Generic;

namespace EulerWasSharp
{
	public class EulerWindow : Gtk.Window
	{
		private	Gtk.HPaned lrPane = new HPaned();
		private Gtk.VBox leftVBox = new VBox();
		private Gtk.Entry problemFilterEntry = new Entry();
		private Gtk.ScrolledWindow nodeViewScroll = new ScrolledWindow();
		private Gtk.NodeView problemNodeView = new NodeView();
		private Gtk.VBox rightVBox = new VBox();
		private Gtk.ScrolledWindow descriptionViewScroll = new ScrolledWindow();
		private Gtk.TextView problemDescriptionTextView = new TextView();
		private Gtk.ScrolledWindow solutionViewScroll = new ScrolledWindow();
		private Gtk.TextView problemSolutionTextView = new TextView();
		private Gtk.HButtonBox actionButtonBox = new HButtonBox();
		private Gtk.Button saveButton = new Button();
		private Gtk.Button calculateButton = new Button();
		
		private Gtk.TextBuffer problemText;
		private Gtk.TextBuffer solutionText;
		private List<ISolvable> problems = new List<ISolvable>();
		private Gtk.NodeStore problemStore = new Gtk.NodeStore(typeof(ProblemNode));
		private ISolvable problem = null;
		
		public EulerWindow() : base(Gtk.WindowType.Toplevel) {
			Build();
			SetupSignals();
			FindProblems();
			FillProblemView();
		}
		
		protected void Build() {
			actionButtonBox.LayoutStyle = Gtk.ButtonBoxStyle.End;
			saveButton.Label = "Save Result!";
			actionButtonBox.Add(saveButton);
			calculateButton.Label = "Calculate!";
			actionButtonBox.Add(calculateButton);
			
			problemText = problemDescriptionTextView.Buffer;
			solutionText = problemSolutionTextView.Buffer;
			problemDescriptionTextView.WrapMode = WrapMode.Word;
			problemSolutionTextView.WrapMode = WrapMode.Word;
			
			problemText.Text = "Select a problem from the left. The description of the problem will appear here.";
			solutionText.Text = "Once you have selected a problem, click \"Calculate!\" below to generate the solution. The solution will appear here." +
				"\nClicking \"Save Result!\" will allow you to save the result in a text file wherever you would like.";
			
			descriptionViewScroll.ShadowType = Gtk.ShadowType.EtchedIn;
			descriptionViewScroll.Add(problemDescriptionTextView);
			rightVBox.PackStart(descriptionViewScroll, true, true, 0);
			solutionViewScroll.ShadowType = Gtk.ShadowType.EtchedIn;
			solutionViewScroll.Add(problemSolutionTextView);
			rightVBox.PackStart(solutionViewScroll, true, true, 0);
			rightVBox.PackStart(actionButtonBox, false, true, 5);
			
			leftVBox.PackStart(problemFilterEntry, false, false, 5);
			nodeViewScroll.ShadowType = Gtk.ShadowType.EtchedIn;
			nodeViewScroll.Add(problemNodeView);
			leftVBox.PackStart(nodeViewScroll, true, true, 5);
			
			lrPane.Pack1(leftVBox, true, false);
			lrPane.Pack2(rightVBox, true, false);
			
			lrPane.Position = 220;
			this.Add(lrPane);
			
			this.SetDefaultSize(900, 600);
			this.ShowAll();
		}

		private void FindProblems()
		{
			//TODO use reflection to find problems automatically
			problems.Add(new Problem1());
			problems.Add(new Problem2());
			problems.Add(new Problem3());
			problems.Add(new Problem4());
			problems.Add(new Problem5());
			problems.Add(new Problem6());
			problems.Add(new Problem7());
			problems.Add(new Problem8());
		}
	
		private void FillProblemView()
		{
			//problemNodeView
			problemNodeView.AppendColumn("Problem", new Gtk.CellRendererText(), "text", 0);
			problems.ForEach(t => problemStore.AddNode(new ProblemNode(t)));
			problemNodeView.NodeStore = problemStore;
			problemNodeView.ShowAll();
		}
		
		protected void SetupSignals()
		{
			DeleteEvent += new DeleteEventHandler(OnDeleteEvent);
			problemNodeView.NodeSelection.Changed += new System.EventHandler(ProblemSelectedHandler);
			saveButton.Clicked += new System.EventHandler(SaveHandler);
			calculateButton.Clicked += new System.EventHandler(CalculateHandler);
		}
		
		protected void OnDeleteEvent(object sender, DeleteEventArgs a)
		{
			Application.Quit();
			a.RetVal = true;
		}
		
		protected void CalculateHandler(object o, System.EventArgs args)
		{
			if (problem != null) {
				solutionText.Text = problem.Solution;
			}
		}
		
		protected void SaveHandler(object o, System.EventArgs args)
		{
		}
	
		protected void ProblemSelectedHandler(object o, System.EventArgs args)
		{
			Gtk.NodeSelection s = (Gtk.NodeSelection)o;
            Console.WriteLine(s);
            Console.WriteLine(s.SelectedNode == null);
			ProblemNode n = (ProblemNode)s.SelectedNode;
			if (n != null)
			{
				problem = n.problem;
				problemText.Text = problem.Description;
			}
		}
		
		public static void Main(string[] args)
		{
			Application.Init();
			EulerWindow win = new EulerWindow();
			win.Show();
			Application.Run();
		}

		[Gtk.TreeNode (ListOnly=true)]
		public class ProblemNode : Gtk.TreeNode {
			public ISolvable problem;
	
			public ProblemNode(ISolvable problem)
			{
				this.problem = problem;
			}
	
			[Gtk.TreeNodeValue (Column=0)]
			public string ProblemName { get { return problem.Name; } }
		}
		}
}