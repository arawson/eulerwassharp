﻿/*
 * Created by SharpDevelop.
 * User: arawson
 * Date: 1/1/2015
 * Time: 4:45 PM
 */
using System;
using System.Collections.Generic;

namespace EulerWasSharp
{
	public class Problem4 : ISolvable
	{
		private bool solved = false;
		private string solution = "";
		
		public string Name
		{
			get { return "Problem 4: Largest Palindrome Product"; }
		}
		
		public string Description
		{
			get { return "A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99."
					+ "\n\nFind the largest palindrome made from the product of two 3-digit numbers."; }
		}
		
		public string Solution {
			get {
				if (!solved) 
				{
					//this is a brute force solution, but i've limited the search space to 100 numbers
					List<int> candidates = new List<int>();
					for (int i = 999; i > 900; i--) {
						for (int j = 999; j > 900; j--) {
							int c = i * j;
							if (Util.IsPalindrome(c)) {
								candidates.Add(c);
							}
						}
					}
					
					candidates.Sort();
					
					solution = "" + candidates[candidates.Count - 1];
					solved = true;
				}
				return solution;
			}
		}
		
		
	}
}
