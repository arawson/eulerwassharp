﻿/*
 * Created by SharpDevelop.
 * User: arawson
 * Date: 1/4/2015
 * Time: 3:40 PM
 */
using System;

namespace EulerWasSharp
{
	public class Problem6 : ISolvable
	{
		bool solved = false;
		int solution = 0;
		
		public string Name {
			get { return "Problem 6: Sum Square Difference"; }
		}
		
		public string Description {
			get { return "The sum of the squares of the first ten natural numbers is,\n"
					+ "1^2 + 2^2 + ... + 10^2 = 385\n\n"
					+ "The square of the sum of the first ten natural numbers is,\n"
					+ "(1 + 2 + ... + 10)^2 = 552 = 3025\n\n"
					+ "Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.\n\n"
					+ "Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.";
			}
		}
		
		public string Solution {
			get {
				if (!solved) {
					//its just one hundred items vs a P time algorithm
					//just brute force it
					int sumofsquares = 0;
					for (int i = 1; i <= 100; i++) {
						sumofsquares += i*i;
					}
					
					int squareofsums = 0;
					for (int i = 1; i <= 100; i++) {
						squareofsums += i;
					}
					squareofsums *= squareofsums;
					
					solution = squareofsums - sumofsquares;
					
					solved = true;
				}
				
				return "" + solution;
			}
		}
	}
}
