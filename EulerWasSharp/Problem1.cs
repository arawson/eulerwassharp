﻿/*
 * Created by SharpDevelop.
 * User: arawson
 * Date: 12/31/2014
 * Time: 6:01 PM
 */
using System;

namespace EulerWasSharp
{
	public class Problem1 : ISolvable
	{
		private bool solved = false;
		private int solution = 0;

		public Problem1 ()
		{
		}

		public string Name {
			get { return "Problem 1: Multiples of 3 and 5"; }
		}

		public String Description {
			get { return "If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9."
					+ "The sum of these multiples is 23. Find the sum of all the multiples of 3 or 5 below 1000."; }
		}

		public String Solution {
			get {
				if (!solved) {
					for (int i = 1; i < 1000; i++) {
						if (i % 3 == 0 || i % 5 == 0) {
							solution += i;
						}
					}
					solved = true;
				}
				return "" + solution;
			}
		}
	}
}
